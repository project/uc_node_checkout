You currently have the 'master' branch of this project checked out. There is
no code in the 'master' branch.

To obtain a working version of this module you must checkout one of the named
branches, such as 6.x-2.x or 7.x-2.x.  Please refer to the 'Git instructions'
tab (http://drupal.org/project/uc_node_checkokut/git-instructions) on the
project page for details.
